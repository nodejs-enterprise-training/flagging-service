'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('YARSAN_SEMENTARA', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      YARSAN_TXNUM: {
        type: Sequelize.STRING
      },
      YARSAN_ID: {
        type: Sequelize.STRING
      },
      YARSAN_KANCAB: {
        type: Sequelize.STRING
      },
      YARSAN_MITRA_ID: {
        type: Sequelize.STRING
      },
      YARSAN_PST_NAMA: {
        type: Sequelize.STRING
      },
      YARSAN_KTPANUM: {
        type: Sequelize.STRING
      },
      YARSAN_PNRM_NOID: {
        type: Sequelize.STRING
      },
      YARSAN_PNRM_NAMA: {
        type: Sequelize.STRING
      },
      YARSAN_PNRM_TLP: {
        type: Sequelize.STRING
      },
      YARSAN_PNRM_HP: {
        type: Sequelize.STRING
      },
      YARSAN_JML_HAK: {
        type: Sequelize.NUMERIC
      },
      YARSAN_JML_POT: {
        type: Sequelize.NUMERIC
      },
      YARSAN_JML_NETT: {
        type: Sequelize.NUMERIC
      },
      YARSAN_JML_BLT: {
        type: Sequelize.NUMERIC
      },
      YARSAN_JML_TRM: {
        type: Sequelize.NUMERIC
      },
      YARSAN_DPS_NO: {
        type: Sequelize.STRING
      },
      YARSAN_DPS_REFF: {
        type: Sequelize.STRING
      },
      YARSAN_DPS_TGL: {
        type: Sequelize.DATE
      },
      YARSAN_DPS_USER_NAME: {
        type: Sequelize.STRING
      },
      YARSAN_NOREK: {
        type: Sequelize.STRING
      },
      YARSAN_REK_ATAS_NAMA: {
        type: Sequelize.STRING
      }
    },{
      freezeTableName: true
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('YARSAN_SEMENTARA');
  }
};