'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('cabang_mitra_bayar', {
      cmb_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      cmb_name: {
        type: Sequelize.STRING
      },
      cmb_child_id: {
        type: Sequelize.STRING
      }
    },{
      freezeTableName: true
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('cabang_mitra_bayar');
  }
};