'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {

    await queryInterface.createTable('cabang_asabri', {
      cb_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      cb_name: {
        type: Sequelize.STRING
      },
      cb_date_insert: {
        type: Sequelize.DATE
      },
      cb_address: {
        type: Sequelize.TEXT
      },
      cb_desc: {
        type: Sequelize.TEXT
      },
      cb_headoffice: {
        type: Sequelize.STRING
      },
      cb_code: {
        type: Sequelize.STRING
      }
    },{
      freezeTableName: true
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('cabang_asabri');
  }
};