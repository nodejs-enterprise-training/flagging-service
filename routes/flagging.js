var express = require('express');
var router = express.Router();
var Flagging = require('../controllers/Flagging');

/* Get payment info. */
router.get('/getpaymentinfo/:paymentCode', function(req, res, next) {
   Flagging.getPaymentInfo(req, res, next);
});

/* Update Flag. */
router.post('/updateflag/:paymentCode', function(req, res, next) {
  Flagging.updateFlag(req, res, next);
});

module.exports = router;
