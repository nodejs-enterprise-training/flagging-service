'use strict';

var utils = require('../utils/writer.js');
var Flagging = require('../service/FlaggingService');

module.exports.getPaymentInfo = function getPaymentInfo (req, res, next) {
  var paymentCode = req.params.paymentCode;
  Flagging.getPaymentInfo(paymentCode)
    .then(function (response) {
      console.log(response);
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updateFlag = function updateFlag (req, res, next) {
  var paymentCode = req.params.paymentCode;
  Flagging.updateFlag(paymentCode)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
