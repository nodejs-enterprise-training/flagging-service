'use strict';

const db = require("../models");
const YARSAN_SEMENTARA = db.YARSAN_SEMENTARA;
const Op = db.Sequelize.Op;
/**
 * Ambil data terkait dengan Payment Code
 * 
 *
 * paymentCode String Payment Code
 * returns PensionInfo
 **/
exports.getPaymentInfo = function(paymentCode) {
  return new Promise(function(resolve, reject) {   
    let sql = `SELECT [YARSAN_TXNUM], [YARSAN_ID], [YARSAN_KANCAB], 
                      [YARSAN_MITRA_ID], [YARSAN_PST_NAMA], [YARSAN_KTPANUM], 
                      [YARSAN_PNRM_NOID], [YARSAN_PNRM_NAMA], [YARSAN_PNRM_TLP], 
                      [YARSAN_PNRM_HP], [YARSAN_JML_HAK], [YARSAN_JML_POT], 
                      [YARSAN_JML_NETT], [YARSAN_JML_BLT], [YARSAN_JML_TRM], 
                      [YARSAN_DPS_NO], [YARSAN_DPS_REFF], [YARSAN_DPS_TGL], 
                      [YARSAN_DPS_USER_NAME], [YARSAN_NOREK], [YARSAN_REK_ATAS_NAMA] 
                FROM [YARSAN_SEMENTARA] AS [YARSAN_SEMENTARA] 
                WHERE [YARSAN_SEMENTARA].[YARSAN_ID] = '${paymentCode}'`;
    db.sequelize.query(sql)
      .then(data => {
        resolve(data[0]);
      })
      .catch(err => {
        resolve();
      });
  });
}


/**
 * Update flag utk Payment Code tertentu
 * 
 *
 * paymentCode String Payment Code
 * returns PaymentFlag
 **/
exports.updateFlag = function(paymentCode) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {"empty": false};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

