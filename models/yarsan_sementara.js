'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class YARSAN_SEMENTARA extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  YARSAN_SEMENTARA.init({
    YARSAN_TXNUM: DataTypes.STRING,
    YARSAN_ID: DataTypes.STRING,
    YARSAN_KANCAB: DataTypes.STRING,
    YARSAN_MITRA_ID: DataTypes.STRING,
    YARSAN_PST_NAMA: DataTypes.STRING,
    YARSAN_KTPANUM: DataTypes.STRING,
    YARSAN_PNRM_NOID: DataTypes.STRING,
    YARSAN_PNRM_NAMA: DataTypes.STRING,
    YARSAN_PNRM_TLP: DataTypes.STRING,
    YARSAN_PNRM_HP: DataTypes.STRING,
    YARSAN_JML_HAK: DataTypes.BIGINT,
    YARSAN_JML_POT: DataTypes.BIGINT,
    YARSAN_JML_NETT: DataTypes.BIGINT,
    YARSAN_JML_BLT: DataTypes.BIGINT,
    YARSAN_JML_TRM: DataTypes.BIGINT,
    YARSAN_DPS_NO: DataTypes.STRING,
    YARSAN_DPS_REFF: DataTypes.STRING,
    YARSAN_DPS_TGL: DataTypes.DATE,
    YARSAN_DPS_USER_NAME: DataTypes.STRING,
    YARSAN_NOREK: DataTypes.STRING,
    YARSAN_REK_ATAS_NAMA: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'YARSAN_SEMENTARA',
  });
  return YARSAN_SEMENTARA;
};