'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class cabang_asabri extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  cabang_asabri.init({
    cb_id: DataTypes.INTEGER,
    cb_name: DataTypes.STRING,
    cb_date_insert: DataTypes.DATE,
    cb_address: DataTypes.TEXT,
    cb_desc: DataTypes.TEXT,
    cb_headoffice: DataTypes.STRING,
    cb_code: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'cabang_asabri',
  });
  return cabang_asabri;
};