'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class cabang_mitra_bayar extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  cabang_mitra_bayar.init({
    cmb_id: DataTypes.INTEGER,
    cmb_name: DataTypes.STRING,
    cmb_child_id: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'cabang_mitra_bayar',
  });
  return cabang_mitra_bayar;
};